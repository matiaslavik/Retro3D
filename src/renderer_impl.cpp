#include "renderer_impl.h"
#include "glm/trigonometric.hpp"
#include "glm/geometric.hpp"
#include "glm/vec3.hpp"
#include "glm/gtx/norm.hpp"
#include <limits>
#include <algorithm>
#include <cstring>

namespace Retro3D
{
    RendererImpl::RendererImpl()
    {
        this->camera = { glm::vec2(1.0, 1.0), glm::vec2(0.0, 1.0), 60.0f, 0.05f, 20.0f };
        this->ceilingTextureIndex = -1;
        this->floorTextureIndex = -1;
    }

    CameraProjection RendererImpl::CalculateCameraProjection(const Camera& camera)
    {
        // Camera view plane calculation: Find the width and world space centre of the view plane.
        //B___a___ _ _   <- view plane (a = half width)
        // \    |    /
        //  \   |   /
        //   \ b|  /  b = near (camera near plane distance)
        //    \ | /
        //     \|/
        //     A      A = FOV (Field Of View) / 2
        // Law of sines: a / sin(A) = b / sin(B)
        //               => a = sin(A) * b / sin(B)
        //  We know B = 180 - 90 - half_fov, because a triangle always has a total of 180 degree angles, and the upper right angle is 90.
        CameraProjection projection;
        projection.camera = camera;
        projection.right = glm::vec2(camera.direction.y, -camera.direction.x);
        // Triangle adds up to 180 degrees => angle = 180 - 90 - half_fov
        const float bdivsinB =  camera.near / glm::sin(glm::radians(180.0f - 90.0f - camera.fov / 2.0f));
        // Law of sines: a / sin(A) = b / sin(B) => a = sin(A) * b / sin(B)
        const float halfWidth = bdivsinB * glm::sin(glm::radians(camera.fov));
        projection.width = halfWidth * 2.0f;
        projection.height = projection.width * (static_cast<float>(framebufferHeight) / framebufferWidth);
        projection.centre = camera.position + camera.direction * camera.near;
        return projection;
    }

    // x: Screen x position
    // h: Height of vertical line to draw
    // u: Texture's uv.u coordinate (x axis).
    void RendererImpl::DrawVertical(unsigned int x, unsigned int h, const Texture& texture, float u)
    {
        if (texture.data.size() == 0)
            return;
        // Clamp wall height to screen size.
        const int visibleHeight = std::min(h, framebufferHeight);
        // Calculate wall's top screen position (may be negative, if wall is taller than screen).
        const int wallTopY = (framebufferHeight - h) / 2;
        // Calculate wall's top/bottom screen Y coordinate, clamped to screen size.
        const int startY = (framebufferHeight - visibleHeight) / 2;
        const int endY = startY + visibleHeight;
        const glm::uvec2 textureSize = texture.GetSize();
        for (int i = startY; i < endY; i++)
        {
            // Get current screen pixel index
            const int pixelIndex = (x + framebufferWidth * (i)) * 4;
            // Get texture uv.v coordinate (normalised).
            const float v = glm::clamp(static_cast<float>(i - wallTopY) / h, 0.0f, 1.0f);
            // Get pixel indices of texture uv. Used for looking up colour in the texture.
            const glm::uvec2 textureIndex(static_cast<int>(u * (textureSize.x-1)), static_cast<int>(v * (textureSize.y-1)));
            // Get colour from texture
            const glm::u8vec4 colour = texture.GetPixel(textureIndex.x, textureIndex.y);
            if (colour.a > 0)
            {
                colourBuffer[pixelIndex] = colour.r;
                colourBuffer[pixelIndex+1] = colour.g;
                colourBuffer[pixelIndex+2] = colour.b;
                colourBuffer[pixelIndex+3] = 255;
            }
        }
    }

    void RendererImpl::DrawCeiling(const CameraProjection& cameraProjection)
    {
        if (ceilingTextureIndex >= textures.size() || textures[ceilingTextureIndex].data.size() == 0)
            return;
        const Texture& ceilingTexture = textures[ceilingTextureIndex];
        const glm::uvec2 textureSize = ceilingTexture.GetSize();
        const glm::vec3 right = glm::vec3(cameraProjection.right, 0.0f);
        const glm::vec3 up = glm::vec3(0.0f, 0.0f, 1.0f);
        glm::vec3 eyePosition = glm::vec3(cameraProjection.camera.position, 0.0f);
        glm::vec3 upperLeftCorner = glm::vec3(cameraProjection.centre - cameraProjection.right * cameraProjection.width / 2.0f, cameraProjection.height / 2.0f);
        for (int iy = 0; iy < framebufferHeight / 2; iy++)
        {
            const float ty = iy / static_cast<float>(framebufferHeight);
            for (int ix = 0; ix < framebufferWidth; ix++)
            {
                const float tx = ix / static_cast<float>(framebufferWidth);
                const glm::vec3 rayOrigin = upperLeftCorner + right * tx * cameraProjection.width - up * ty * cameraProjection.height;
                const glm::vec3 rayDirection = glm::normalize(rayOrigin - eyePosition);
                const float t = (0.5f - rayOrigin.z) / rayDirection.z;
                const glm::vec3 intersection = rayOrigin + rayDirection * t;
                const glm::vec2 uv = intersection - glm::floor(intersection);
                const glm::uvec2 textureIndex(static_cast<int>(uv.x * (textureSize.x-1)), static_cast<int>(uv.y * (textureSize.y-1)));
                const glm::u8vec4 colour = ceilingTexture.GetPixel(textureIndex.x, textureIndex.y);
                const int pixelIndex = (ix + framebufferWidth * (iy)) * 4;
                colourBuffer[pixelIndex] = colour.r;
                colourBuffer[pixelIndex+1] = colour.g;
                colourBuffer[pixelIndex+2] = colour.b;
                colourBuffer[pixelIndex+3] = 255;
            }
        }
    }

    void RendererImpl::DrawFloor(const CameraProjection& cameraProjection)
    {
        if (floorTextureIndex >= textures.size() || textures[floorTextureIndex].data.size() == 0)
            return;
        const Texture& floorTexture = textures[floorTextureIndex];
        const glm::uvec2 textureSize = floorTexture.GetSize();
        const glm::vec3 right = glm::vec3(cameraProjection.right, 0.0f);
        const glm::vec3 up = glm::vec3(0.0f, 0.0f, 1.0f);
        glm::vec3 eyePosition = glm::vec3(cameraProjection.camera.position, 0.0f);
        glm::vec3 upperLeftCorner = glm::vec3(cameraProjection.centre - cameraProjection.right * cameraProjection.width / 2.0f, cameraProjection.height / 2.0f);
        for (int iy = framebufferHeight / 2; iy < framebufferHeight; iy++)
        {
            const float ty = iy / static_cast<float>(framebufferHeight);
            for (int ix = 0; ix < framebufferWidth; ix++)
            {
                const float tx = ix / static_cast<float>(framebufferWidth);
                const glm::vec3 rayOrigin = upperLeftCorner + right * tx * cameraProjection.width - up * ty * cameraProjection.height;
                const glm::vec3 rayDirection = glm::normalize(rayOrigin - eyePosition);
                const float t = (-0.5f - rayOrigin.z) / rayDirection.z;
                const glm::vec3 intersection = rayOrigin + rayDirection * t;
                const glm::vec2 uv = intersection - glm::floor(intersection);
                const glm::uvec2 textureIndex(static_cast<int>(uv.x * (textureSize.x-1)), static_cast<int>(uv.y * (textureSize.y-1)));
                const colour_t colour = floorTexture.GetPixel(textureIndex.x, textureIndex.y);
                const int pixelIndex = (ix + framebufferWidth * (iy)) * 4;
                colourBuffer[pixelIndex] = colour.r;
                colourBuffer[pixelIndex+1] = colour.g;
                colourBuffer[pixelIndex+2] = colour.b;
                colourBuffer[pixelIndex+3] = 255;
            }
        }
    }

    void RendererImpl::DrawWalls(const CameraProjection& cameraProjection)
    {
        Camera camera = cameraProjection.camera;
        const float viewRange = camera.far - camera.near;
        for (int iPixel = 0; iPixel < framebufferWidth; iPixel++)
        {
            const float tPixel = iPixel / (framebufferWidth - 1.0f) - 0.5f;
            const glm::vec2 pixelPos = cameraProjection.centre + tPixel * cameraProjection.width * cameraProjection.right;
            const glm::vec2 rayDir = glm::normalize(pixelPos - camera.position);
            const float cosAngle = glm::dot(rayDir, camera.direction);

            // Calculate offset between ray intersections on grid X and Y lines
            const float xIntersectionOffset = 1.0f / std::max(std::abs(rayDir.x), 0.00001f);
            const float yIntersectionOffset = 1.0f / std::max(std::abs(rayDir.y), 0.00001f);
            // Calculate next X and Y intersection
            float nextXIntersection = std::numeric_limits<float>::max();
            float nextYIntersection = std::numeric_limits<float>::max();
            if (std::abs(rayDir.x) > 0.0001f)
            {
                const float edgeX = rayDir.x > 0.0f ? std::floor(pixelPos.x + 1.0f) : std::floor(pixelPos.x);
                nextXIntersection = (edgeX - pixelPos.x) / rayDir.x;
            }
            if (std::abs(rayDir.y) > 0.0001f)
            {
                const float edgeY = rayDir.y > 0.0f ? std::floor(pixelPos.y + 1.0f) : std::floor(pixelPos.y);
                nextYIntersection = (edgeY - pixelPos.y) / rayDir.y;
            }

            // Raytrace 2D grid
            float t = 0.0f;
            float depth = std::numeric_limits<float>::max();
            while(t < viewRange)
            {
                // Pick nearest intersection
                if (nextXIntersection < nextYIntersection)
                {
                    t = nextXIntersection;
					nextXIntersection += xIntersectionOffset;
                }
                else
                {
                    t = nextYIntersection;
					nextYIntersection += yIntersectionOffset;
                }

                // Calculate current position
                const glm::vec2 rayPos = pixelPos + rayDir * (t * 1.001f);
                const int mapX = static_cast<int>(glm::floor(rayPos.x));
                const int mapY = static_cast<int>(glm::floor(rayPos.y));
                const int mapIndex = mapX + mapY * static_cast<int>(mapWidth);
                // Look up current cell, and check if it is a wall.
                if (mapX >= 0 && mapY >= 0 && mapX < mapWidth && mapY < mapHeight && map[mapIndex] >= 0 && map[mapIndex] < textures.size())
                {
                    // Get the wall colour
                    const int16_t mapValue = map[mapIndex];
                    // Calculate wall texture's uv.u coordinate based on distance from grid X and Y edge.
                    // Since we don't have diagonal walls, we can use u = xOffset + yOffset.
                    const float u = rayPos.x - mapX + rayPos.y - mapY;
                    depth = glm::length(rayPos - camera.position);
                    // Calculate projected wall height. Multiply distance by cos(angle) to avoid "fish eye" effect
                    const float wallHeightProj = camera.near / (depth * cosAngle);
                    // CAlculate screen space pixel height of wall.
                    const int wallHeightScreen = static_cast<int>((wallHeightProj / cameraProjection.height) * framebufferHeight);
                    // Draw wall as a 1 pixel wide line, with height relative to distance (depth)
                    if (textures[mapValue].data.size() != 0)
                    {
                        DrawVertical(iPixel, wallHeightScreen, textures[mapValue], u);
                        break;
                    }
                }
            }
            depthBuffer[iPixel] = depth;
        }
    }

    void RendererImpl::DrawSprites(CameraProjection& cameraProjection)
    {
        // Sort sprites by distance to camera
        std::sort(sprites.begin(), sprites.end(), [cameraProjection](SpriteData const& left, SpriteData const& right)
        {
            return glm::length2(glm::vec2(left.x, left.y) - cameraProjection.centre) > glm::length2(glm::vec2(right.x, right.y) - cameraProjection.centre);
        });

        for (const SpriteData& sprite : sprites)
        {
            glm::vec2 spritePosition(sprite.x, sprite.y);
            const Texture& spriteTexture = textures[sprite.textureIndex];
            const glm::vec2 spriteDir = glm::normalize(spritePosition - cameraProjection.centre);

            const float spriteDist = glm::length(spritePosition - cameraProjection.centre);
            // Angle between view direction and direction to sprite centre
            const float spriteAngle = glm::acos(glm::dot(spriteDir, cameraProjection.camera.direction));
            // Triangle adds up to 180 degrees => angle = 180 - 90 - half_fov
            const float bdivsinB =  camera.near / glm::sin(glm::radians(180.0f - 90.0f) - spriteAngle);
            // Law of sines: a / sin(A) = b / sin(B) => a = sin(A) * b / sin(B)
            const float spriteDistOpp = bdivsinB * glm::sin(spriteAngle);
            // t value relative to camera width, in range [0,1]
            const float tSpriteProj = 0.5f + (spriteDistOpp / cameraProjection.width) * glm::sign(glm::dot(spriteDir, cameraProjection.right));

            if (spriteAngle > 1.5708 /* 90 degrees */)
                continue;

            const float spriteDepth = glm::length(spritePosition - camera.position);
            // Sprite centre's screen X index
            const int spriteScreenIndex = glm::floor(tSpriteProj * framebufferWidth);
            // Sprite's screen size
            const int spriteScreenSize = glm::floor(framebufferHeight / spriteDepth);
            // Screen space X position of sprite's left side
            const int spriteLeftIndex = spriteScreenIndex - spriteScreenSize / 2;
            
            const int screenStartX = std::max(0, spriteLeftIndex);
            const int screenEndX = std::min(static_cast<int>(framebufferWidth) - 1, spriteLeftIndex + spriteScreenSize);
            const glm::uvec2 textureSize = spriteTexture.GetSize();
            for (int ix = screenStartX; ix < screenEndX; ix++)
            {
                if (spriteDist < depthBuffer[ix])
                {
                    const float tx = (ix - spriteLeftIndex) / static_cast<float>(spriteScreenSize);
                    DrawVertical(ix, spriteScreenSize, spriteTexture, tx);
                }
            }
        }
    }

    void RendererImpl::Update(float deltaTime)
    {
        const unsigned int frameBufferDimension = framebufferHeight * framebufferWidth;
        const unsigned int frameBufferSize = frameBufferDimension * 4;
        if (frameBufferSize != colourBuffer.size())
        {
            colourBuffer.resize(frameBufferSize);
            depthBuffer.resize(frameBufferDimension);
        }

        CameraProjection cameraProjection = CalculateCameraProjection(this->camera);

        DrawCeiling(cameraProjection);
        DrawFloor(cameraProjection);
        DrawWalls(cameraProjection);
        DrawSprites(cameraProjection);
    }

    void RendererImpl::SetCamera(float x, float y, float dirX, float dirY)
    {
        this->camera.position = glm::vec2(x, y);
        this->camera.direction = glm::vec2(dirX, dirY);
    }

    void RendererImpl::SetResolution(unsigned int width, unsigned int height)
    {
        framebufferWidth = width;
        framebufferHeight = height;
    }

    void RendererImpl::SetMap(int16_t* map, unsigned int width, unsigned int height)
    {
        this->mapWidth = width;
        this->mapHeight = height;
        unsigned int length = mapWidth * mapHeight;
        this->map.resize(length);
        std::memcpy(this->map.data(), map, length * sizeof(int16_t));
    }

    void RendererImpl::SetSprites(const SpriteData* sprites, uint32_t count)
    {
        this->sprites.clear();
        for (int i = 0; i < count; i++)
            this->sprites.push_back(sprites[i]);
    }

    void RendererImpl::SetTexture(unsigned int index, uint8_t* data, uint32_t length, unsigned int width, unsigned int height)
    {
        if (textures.size() <= index)
            textures.resize(index + 1);
        Texture& texture = textures[index];
        texture.width = width;
        texture.height = height;
        texture.data.resize(length);
        std::memcpy(texture.data.data(), data, length);
    }

    void RendererImpl::SetCeilingTexture(unsigned int index)
    {
        ceilingTextureIndex = index;
    }

    void RendererImpl::SetFloorTexture(unsigned int index)
    {
        floorTextureIndex = index;
    }

    const uint8_t* RendererImpl::GetFrameBufferPtr()
    {
        return colourBuffer.data();
    }

    uint32_t RendererImpl::GetFrameBufferSize()
    {
        return colourBuffer.size();
    }
}
