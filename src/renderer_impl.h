#pragma once
#include "glm/vec2.hpp"
#include "glm/vec4.hpp"
#include "types.h"
#include <cstdint>
#include <vector>

namespace Retro3D
{
    typedef glm::tvec4<uint8_t> colour_t;

    struct Camera
    {
        // World space position of player.
        glm::vec2 position;
        // World space direction (where we're looking).
        glm::vec2 direction;
        // Field Of View.
        float fov;
        // Distance to the near clipping plane.
        float near;
        // Distance to the far clipping plane (maximum view distance).
        float far;
    };

    struct CameraProjection
    {
        Camera camera;
        // The width of the camera view plane (the plane we will cast rays through).
        float width;
        // The height of the camera view plane.
        float height;
        // The world position of the centre of the camera view plane.
        glm::vec2 centre;
        // World space right vector (pointing to the right of the camera).
        glm::vec2 right;
    };

    class Texture
    {
    public:
        std::vector<uint8_t> data;
        unsigned int width;
        unsigned int height;

        inline glm::uvec2 GetSize() const
        {
            return glm::uvec2(width, height);
        }

        inline colour_t GetPixel(unsigned int x, unsigned int y) const
        {
            const unsigned int index = (x + y * width) * 4;
            const uint8_t* pixels = data.data() + index;
            return colour_t(pixels[0], pixels[1], pixels[2], pixels[3]);
        }
    };

    class RendererImpl
    {
    private:
        // Colour framebuffer, containing the final pixels we will draw on the screen.
        std::vector<uint8_t> colourBuffer;
        // Colour framebuffer, containing the final pixels we will draw on the screen.
        std::vector<float> depthBuffer;

        // Camera (for player).
        Camera camera;

        float movementSpeed = 4.0;
        float rotationSpeed = 3.0;

        unsigned int framebufferWidth = 640;
        unsigned int framebufferHeight = 400;

        // Dungeon map (0 = nothing, 1,2,3 = wall - index is used to look up texture in "wallTextures" array).
        std::vector<int16_t> map;
        unsigned int mapWidth;
        unsigned int mapHeight;

        std::vector<SpriteData> sprites;

        // Wall textures. Index is value from map.
        std::vector<Texture> textures;
        unsigned int ceilingTextureIndex;
        unsigned int floorTextureIndex;

        // Calculate the projection params we will need for raycasting (camera view plane width and centre).
        CameraProjection CalculateCameraProjection(const Camera& camera);

        // Draw a vertical line with a specified image and height, from the vertical centre of the screen.
        void DrawVertical(unsigned int x, unsigned int h, const Texture& texture, float u);
        // Draw the ceiling.
        void DrawCeiling(const CameraProjection& cameraProjection);
        // Draw the floor.
        void DrawFloor(const CameraProjection& cameraProjection);
        // Raycast map and draw walls.
        void DrawWalls(const CameraProjection& cameraProjection);
        // Render billboarded sprites
        void DrawSprites(CameraProjection& cameraProjection);

    public:
        RendererImpl();
        void Update(float deltaTime);
        void SetCamera(float x, float y, float dirX, float dirY);
        void SetResolution(unsigned int width, unsigned int height);
        void SetMap(int16_t* map, unsigned int width, unsigned int height);
        void SetSprites(const SpriteData* sprites, uint32_t count);
        void SetTexture(unsigned int index, uint8_t* data, uint32_t length, unsigned int width, unsigned int height);
        void SetCeilingTexture(unsigned int index);
        void SetFloorTexture(unsigned int index);
        const uint8_t* GetFrameBufferPtr();
        uint32_t GetFrameBufferSize();
    };
}
