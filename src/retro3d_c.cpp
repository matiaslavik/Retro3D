#include "retro3d_c.h"
#include "retro3d.h"

RETRO3D_API C_STRUCT Renderer* CreateRenderer()
{
    Retro3D::Renderer* renderer = new Retro3D::Renderer();
    return reinterpret_cast<Renderer*>(renderer);
}

RETRO3D_API void Update(Renderer* renderer, float deltaTime)
{
    reinterpret_cast<Retro3D::Renderer*>(renderer)->Update(deltaTime);
}

RETRO3D_API void SetCamera(Renderer* renderer, float x, float y, float dirX, float dirY)
{
    reinterpret_cast<Retro3D::Renderer*>(renderer)->SetCamera(x, y, dirX, dirY);
}

RETRO3D_API void SetResolution(Renderer* renderer, unsigned int width, unsigned int height)
{
    reinterpret_cast<Retro3D::Renderer*>(renderer)->SetResolution(width, height);
}

RETRO3D_API void SetMap(Renderer* renderer, int16_t* map, unsigned int width, unsigned int height)
{
    reinterpret_cast<Retro3D::Renderer*>(renderer)->SetMap(map, width, height);
}

RETRO3D_API void SetSprites(Renderer* renderer, C_STRUCT SpriteData* sprites, uint32_t count)
{
    reinterpret_cast<Retro3D::Renderer*>(renderer)->SetSprites(sprites, count);
}

RETRO3D_API void SetTexture(Renderer* renderer, unsigned int index, uint8_t* data, uint32_t length, unsigned int width, unsigned int height)
{
    reinterpret_cast<Retro3D::Renderer*>(renderer)->SetTexture(index, data, length, width, height);
}

RETRO3D_API void SetCeilingTexture(Renderer* renderer, unsigned int index)
{
    reinterpret_cast<Retro3D::Renderer*>(renderer)->SetCeilingTexture(index);
}

RETRO3D_API void SetFloorTexture(Renderer* renderer, unsigned int index)
{
    reinterpret_cast<Retro3D::Renderer*>(renderer)->SetFloorTexture(index);
}

RETRO3D_API const uint8_t* GetFrameBufferPtr(Renderer* renderer)
{
    return reinterpret_cast<Retro3D::Renderer*>(renderer)->GetFrameBufferPtr();
}

RETRO3D_API uint32_t GetFrameBufferSize(Renderer* renderer)
{
    return reinterpret_cast<Retro3D::Renderer*>(renderer)->GetFrameBufferSize();
}
