#include "retro3d.h"
#include "renderer_impl.h"

namespace Retro3D
{
    Renderer::Renderer()
    {
        impl = new RendererImpl();
    }

    Renderer::~Renderer()
    {
        delete impl;
    }

    void Renderer::Update(float deltaTime)
    {
        impl->Update(deltaTime);
    }

    void Renderer::SetCamera(float x, float y, float dirX, float dirY)
    {
        impl->SetCamera(x, y, dirX, dirY);
    }

    void Renderer::SetResolution(unsigned int width, unsigned int height)
    {
        impl->SetResolution(width, height);
    }

    void Renderer::SetMap(int16_t* map, unsigned int width, unsigned int height)
    {
        impl->SetMap(map, width, height);
    }

    void Renderer::SetSprites(SpriteData* sprites, uint32_t count)
    {
        impl->SetSprites(sprites, count);
    }

    void Renderer::SetTexture(unsigned int index, uint8_t* data, uint32_t length, unsigned int width, unsigned int height)
    {
        impl->SetTexture(index, data, length, width, height);
    }

    void Renderer::SetCeilingTexture(unsigned int index)
    {
        impl->SetCeilingTexture(index);
    }

    void Renderer::SetFloorTexture(unsigned int index)
    {
        impl->SetFloorTexture(index);
    }

    const uint8_t* Renderer::GetFrameBufferPtr()
    {
        return impl->GetFrameBufferPtr();
    }

    uint32_t Renderer::GetFrameBufferSize()
    {
        return impl->GetFrameBufferSize();
    }
}
