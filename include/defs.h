#if defined(_MSC_VER)
#ifdef RETRO3D_EXPORTS
#      define RETRO3D_API __declspec(dllexport)
#    else
#      define RETRO3D_API __declspec(dllimport)
#    endif
#elif defined(__GNUC__)
	#define RETRO3D_API __attribute__((visibility("default")))
#else
	#define RETRO3D_API
#endif

#ifdef __cplusplus
#define C_STRUCT
#else
#define C_STRUCT struct
#endif
