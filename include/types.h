#ifndef RETRO3D_TYPES_H
#define RETRO3D_TYPES_H

struct SpriteData
{
    float x;
    float y;
    unsigned int textureIndex;
};
#endif
