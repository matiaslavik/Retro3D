#ifndef RETRO3D_C_H
#define RETRO3D_C_H
#include "defs.h"
#include "types.h"
#include <stdint.h>
#ifdef __cplusplus
extern "C" {
#endif

struct Renderer;

RETRO3D_API C_STRUCT Renderer* CreateRenderer();
RETRO3D_API void Update(Renderer* renderer, float deltaTime);
RETRO3D_API void SetCamera(Renderer* renderer, float x, float y, float dirX, float dirY);
RETRO3D_API void SetResolution(Renderer* renderer, unsigned int width, unsigned int height);
RETRO3D_API void SetMap(Renderer* renderer, int16_t* map, unsigned int width, unsigned int height);
RETRO3D_API void SetSprites(Renderer* renderer, C_STRUCT SpriteData* sprites, uint32_t count);
RETRO3D_API void SetTexture(Renderer* renderer, unsigned int index, uint8_t* data, uint32_t length, unsigned int width, unsigned int height);
RETRO3D_API void SetCeilingTexture(Renderer* renderer, unsigned int index);
RETRO3D_API void SetFloorTexture(Renderer* renderer, unsigned int index);
RETRO3D_API const uint8_t* GetFrameBufferPtr(Renderer* renderer);
RETRO3D_API uint32_t GetFrameBufferSize(Renderer* renderer);

#ifdef __cplusplus
}
#endif
#endif
