#pragma once
#include "defs.h"
#include "types.h"
#include <cstdint>

namespace Retro3D
{
    class RendererImpl;

    class Renderer
    {
    private:
        RendererImpl* impl;
    
    public:
        Renderer();
        ~Renderer();
        void Update(float deltaTime);
        void SetCamera(float x, float y, float dirX, float dirY);
        void SetResolution(unsigned int width, unsigned int height);
        void SetMap(int16_t* map, unsigned int width, unsigned int height);
        void SetSprites(SpriteData* sprites, uint32_t count);
        void SetTexture(unsigned int index, uint8_t* data, uint32_t length, unsigned int width, unsigned int height);
        void SetCeilingTexture(unsigned int index);
        void SetFloorTexture(unsigned int index);
        const uint8_t* GetFrameBufferPtr();
        uint32_t GetFrameBufferSize();
    };
}
