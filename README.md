# Retro3D

Pseudo-3D software rendering library (WIP).

For an example of how to use this library, see [this SFML sample project](https://codeberg.org/matiaslavik/Retro3D-SFML-Example).
